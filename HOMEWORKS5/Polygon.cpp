#include "Polygon.h"


// Constructor
/*
This function creates a new empty Polygon
*/
Polygon::Polygon(const std::string type, const std::string name) : Shape(type, name) {};

// Destructor
/*
This function deletes all the points in a polygon
*/
Polygon::~Polygon() {
	for (int i = 0; i < size(_points); i++) {
		deletePoint(i);
	}
}

// Methods
/*
This function adds a new point into the vector
*/
void Polygon::addPoint(const Point p) {
	_points.push_back(p);
}
/*
This function deletes a new point into the vector
*/
void Polygon::deletePoint(int i) {
	_points.erase(_points.begin() + i);
}
/*
This function changes a spcific point
*/
void Polygon::changePoint(int i, Point p) {
	_points[i] = p;
}

// Getters
std::vector<Point> Polygon::getPoints() const {
	return _points;
}
