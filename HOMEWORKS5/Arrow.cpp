#include "Arrow.h"


// Constructor

/*
This function creates a new arrow using 2 points
*/
Arrow::Arrow(Point a, const Point b, const std::string type, const std::string name) : Shape(name, type) {
	if (a == b) {
		std::cout << "The two points are the same, exiting...";
		exit(1);
	}
	_source = a;
	_destination = b;
}

// Destructor
Arrow:: ~Arrow() {
	delete& _source;
	delete& _destination;
}

// Getters
Point Arrow::getSource() const {
	return _source;
}
Point Arrow::getDestination() const {
	return _destination;
}


void Arrow::draw(const Canvas& canvas)
{

	canvas.draw_arrow(_source, _destination);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_source, _destination);
}

/*
This function returns the area of the arrow, which is always 0
*/
double Arrow::getArea() {
	return 0;
}

/*
This function returns the perimeter of the arrow, which is the distance between the points
*/
double Arrow::getPerimeter() {
	return _source.distance(_destination);
}
/*
This function adds a specific point to each point in the arrow
*/
void Arrow::move(Point other) { // add the Point to all the points of shape
	_source += other;
	_destination += other;
}
/*
This function prints details about the arrow
*/

void Arrow::printDetails() {
	std::cout << "Arrow Data:" << std::endl;
	_source.printData();
	_destination.printData();
}

