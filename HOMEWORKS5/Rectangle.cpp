#include "Rectangle.h"


// Constructor

/*
This function creates a new rect by a base point, its length and its width.
*/
myShapes::Rectangle::Rectangle(Point a, const double length, const double width, const std::string type, const std::string name) : Polygon(type, name) {

	if (width <= 0 || length <= 0) {
		std::cout << "Error, one of the rectengle fields is less or equal to 0";
		exit(1);
	}

	Polygon::addPoint(a);
	Point* point1 = new Point(a.getX() + width, a.getY());
	Polygon::addPoint(*point1);
	Point* point2 = new Point(a.getX(), a.getY() + length);
	Polygon::addPoint(*point2);
	Point* point3 = new Point(a.getX() + width, a.getY() + length);
	Polygon::addPoint(*point3);

}

// Destructor
myShapes::Rectangle::~Rectangle() {
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		delete &Polygon::getPoints()[i];
	}
}; // the Polygon destructor will delete everything.

// Methods

// override functions if need (virtual + pure virtual)

/*
This function calculates the area, by multipling one side length by the other
*/
double myShapes::Rectangle::getArea() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];
	Point d = Polygon::getPoints()[3];
	return (b.getX() - a.getX()) * (d.getX() - c.getX());
}
/*
This function calculates the perimeter, by adding  one side length to the other twice
*/
double myShapes::Rectangle::getPerimeter() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];
	Point d = Polygon::getPoints()[3];

	return (b.getX() - a.getX()) + (d.getX() - c.getX()) * 2;

}
/*
This function moves all the rect by a specific point
*/
void myShapes::Rectangle::move(Point other) {
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		Polygon::changePoint(i, Polygon::getPoints()[i] + other);
	}
}
/*
This function print details about the rectangle
*/
void myShapes::Rectangle::printDetails() {
	std::cout << "Rectengle Data:" << std::endl;
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		Polygon::getPoints()[i].printData();
	}
}


void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


