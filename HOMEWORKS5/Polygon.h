#pragma once

#include "Shape.h"

#include <vector>

class Polygon : public Shape
{
public:

	// Constructor
	Polygon(const std::string type, const std::string name);

	// Destructor
	~Polygon();

	// Methods

	void addPoint(const Point p);
	void deletePoint(int i);
	void changePoint(int i, Point p);

	// Getters
	std::vector<Point> getPoints() const;


protected:
	std::vector<Point> _points;
};