#include "Point.h"

#pragma once

/*
This function creates a new point in the (0,0) point
*/
Point::Point() {
	_x = 0;
	_y = 0;
}

/*
This function creates a new point in the by using the given 2 points
*/
Point::Point(const double x, const double y) {
	_x = x;
	_y = y;
}
/*
This function deletes a point
*/
Point::~Point() {
	_x = 0;
	_y = 0;
}

// Operators


Point Point::operator+(Point& other) {
	Point copy(getX(), getY());
	copy += other;
	return copy;

}

Point& Point::operator+=(Point& other) {
	this->_x += other.getX();
	this->_y += other.getY();
	return *this;
}
/*
This function checks if 2 points r the same
*/
bool Point::operator==(const Point& other) {
	return (this->_x == other.getX()) && (this->_y == other.getY());

}
// Getters
double Point::getX() const {
	return _x;
}
double Point::getY() const {
	return _y;
}

/*
This function prints data about the point
*/
void Point::printData() const {
	std::cout << "Point: (" << std::to_string(this->_x) << "," << std::to_string(_y) << ")\n";
}
/*
This function calculates the distance between 2 points by using the Pitagoras Theroem
*/
double Point::distance(const Point& other) const {
	return sqrt(abs(getY() - other.getY()) + abs(getX() - other.getX()));
}
