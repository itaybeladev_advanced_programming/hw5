#pragma once
#include "Shape.h"
#include "Point.h"
class Arrow : public Shape
{
public:

	// Constructor
	Arrow(const Point a, const Point b, const std::string type, const std::string name);

	// Destructor
	~Arrow();

	// Getters
	Point getSource() const;
	Point getDestination() const;

	// Methods
	virtual double getArea() override;
	virtual double getPerimeter() override;
	virtual void move(Point other) override;
	virtual void printDetails() override;

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	//override functions if need (virtual + pure virtual)

private:
	Point _source;
	Point _destination;
};