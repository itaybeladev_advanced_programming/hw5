#pragma once
#include "Shape.h"
#include "Canvas.h"
#include "Arrow.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Quadrangle.h"
#include <vector>

enum { ARROW = 1, CIRCLE, TRIANGLE, RECTANGLE , QUADRANGLE};
enum { ADD = 1, GET, DEL_ALL, EXIT };
enum { MOVE_SHAPE = 1, GET_INFO, REMOVE };


class Menu
{
public:

	Menu();
	~Menu();

	void askAndCallAction();

	void addNewShape();
	void getDataOrDeleteShape();



	void createArrow(std::string name);
	void createCircle(std::string name);
	void createTriangle(std::string name);
	void createRectangle(std::string name);
	void createQuadrangle(std::string name);

	void moveShape(int shapeId);
	void getShapeInfo(int shapeId);
	void deleteShape(int shapeId);
	void deleteAllShapes();
	void ExitMenu();
	// more functions..



private:
	Canvas _canvas;
	std::vector<Shape*> _shapes;

};