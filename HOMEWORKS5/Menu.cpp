#include "Menu.h"


/*
Those are array of functions used to make the program more clean
*/
static void (Menu::* menuFunctions[4])() = { &Menu::addNewShape , &Menu::getDataOrDeleteShape, &Menu::deleteAllShapes, &Menu::ExitMenu };
static void(Menu::* shapesAdder[5])(std::string) = { &Menu::createArrow , &Menu::createCircle , &Menu::createTriangle ,&Menu::createRectangle ,&Menu::createQuadrangle};
static void(Menu::* changeShape[3])(int) = { &Menu::moveShape, &Menu::getShapeInfo, &Menu::deleteShape };


Menu::Menu()
{
	//_canvas = *new Canvas();
	while (1) {
		askAndCallAction();
	}
}
Menu::~Menu() {
	Menu::deleteAllShapes();
}

/*
This function asks the user for action and calls the coorosponding function
*/
void Menu::askAndCallAction() {

	int option = 0;
	while (option < ADD || option > EXIT) {
		std::cout << "Please enter what action you want to do:" << std::endl;
		std::cout << "[1]\tAdd new shape" << std::endl;
		std::cout << "[2]\tGet data/change existing shape" << std::endl;
		std::cout << "[3]\tDelete all shapes" << std::endl;
		std::cout << "[4]\tExit" << std::endl;

		std::cin >> option;
	}
	(this->*menuFunctions[option - 1])();
}

/*
This function asks the user for a shape type, and returns its id
*/
int askForShape() {
	int shapeId = 0;
	while (shapeId < ARROW || shapeId > QUADRANGLE) {
		std::cout << "Please enter which shape you want to add:" << std::endl;
		std::cout << "[1]\tArrow" << std::endl;
		std::cout << "[2]\tCircle" << std::endl;
		std::cout << "[3]\tTriangle" << std::endl;
		std::cout << "[4]\tRectangle" << std::endl;
		std::cout << "[5]\tQuadrangle" << std::endl;
		std::cin >> shapeId;
	}
	return shapeId;
}

/*
This function adds a new shape into the vector, given its name, and other details in the class.
*/
void Menu::addNewShape() {
	int shapeId = askForShape();

	std::string name = "";

	std::cout << "Please enter the name of the shape:" << std::endl;
	std::cin >> name;


	(this->*shapesAdder[shapeId - 1])(name);
	_shapes[_shapes.size() - 1]->draw(_canvas);

}

/*
This function creates a new arrow instance from the user's input
*/
void Menu::createArrow(std::string name) {
	int x = 0;
	int y = 0;
	std::cout << "Please enter the first point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the first point y-coordinate:" << std::endl;
	std::cin >> y;

	Point a(x, y);

	std::cout << "Please enter the second point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the second point y-coordinate:" << std::endl;
	std::cin >> y;

	Point b(x, y);

	Arrow* newArrow = new Arrow(a, b, "Arrow", name);
	_shapes.push_back(newArrow);
}

/*
This function creates a new circle instance from the user's input
*/
void Menu::createCircle(std::string name) {
	int x = 0;
	int y = 0;
	int radius = 0;
	std::cout << "Please enter the x-coordinate of the center of the circle:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the y-coordinate of the center of the circle:" << std::endl;
	std::cin >> y;

	Point center(x, y);

	std::cout << "Please enter the radius of the circle:" << std::endl;
	std::cin >> radius;

	Circle* newCircle = new Circle(center, radius, "Circle", name);
	_shapes.push_back(newCircle);
}


/*
This function creates a new triangle instance from the user's input
*/
void Menu::createTriangle(std::string name) {
	int x = 0;
	int y = 0;

	std::cout << "Please enter the first point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the first point y-coordinate:" << std::endl;
	std::cin >> y;

	Point a(x, y);

	std::cout << "Please enter the second point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the second point y-coordinate:" << std::endl;
	std::cin >> y;

	Point b(x, y);

	std::cout << "Please enter the third point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the third point y-coordinate:" << std::endl;
	std::cin >> y;

	Point c(x, y);
	Triangle* newTriangle = new Triangle(a, b, c, "Triangle", name);
	_shapes.push_back(newTriangle);

}

/*
This function creates a new rectangle instance from the user's input
*/
void Menu::createRectangle(std::string name) {
	int x = 0;
	int y = 0;
	int width = 0;
	int length = 0;

	std::cout << "Please enter the first point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the first point y-coordinate:" << std::endl;
	std::cin >> y;

	Point a(x, y);

	std::cout << "Please enter the length of the rect:" << std::endl;
	std::cin >> length;

	std::cout << "Please enter the width of the rect:" << std::endl;
	std::cin >> width;
	myShapes::Rectangle* newRectangle = new myShapes::Rectangle(a, length, width, "Rectangle", name);
	_shapes.push_back(newRectangle);

}
/*
This function creates a new Quadrangle instance from the user's input
*/
void Menu::createQuadrangle(std::string name) {
	int x = 0;
	int y = 0;

	std::cout << "Please enter the first point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the first point y-coordinate:" << std::endl;
	std::cin >> y;

	Point a(x, y);

	std::cout << "Please enter the second point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the second point y-coordinate:" << std::endl;
	std::cin >> y;

	Point b(x, y);

	std::cout << "Please enter the third point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the third point y-coordinate:" << std::endl;
	std::cin >> y;
	Point c(x, y);

	std::cout << "Please enter the fourth point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the fourth point y-coordinate:" << std::endl;
	std::cin >> y;

	Point d(x, y);
	Quadrangle* newQuad = new Quadrangle(a, b, c,d, "Quadrangle", name);
	_shapes.push_back(newQuad);

}
/*
This function handles deleteing and recieveing data about certain shapes
*/
void Menu::getDataOrDeleteShape() {
	int shapeId = 0;
	int action = 0;
	std::cout << "Please choose a shape from the existing shapes:" << std::endl;
	for (int i = 0; i < _shapes.size(); i++) {
		std::cout << "Shape number " << std::to_string(i + 1) << ": " << std::endl;
		_shapes[i]->printDetails();
	}

	std::cin >> shapeId;

	while (shapeId < 1 || shapeId > _shapes.size()) {
		std::cout << "Invalid shape id, try again:" << std::endl;
		std::cin >> shapeId;
	}
	std::cout << "Please enter what you want to do with this shape:" << std::endl;
	std::cout << "[1]\tMove shape" << std::endl;
	std::cout << "[2]\tGet Info about this shape" << std::endl;
	std::cout << "[3]\tDelete Shape" << std::endl;
	std::cout << "[4]\tRectangle" << std::endl;

	std::cin >> action;
	while (action < MOVE_SHAPE || shapeId > REMOVE) {
		std::cout << "Invalid action id, try again:" << std::endl;
		std::cin >> action;
	}
	(this->*changeShape[action - 1])(shapeId);
}

/*
This function moves a specific shape by a specific point
*/
void Menu::moveShape(int shapeId) {

	int x = 0;
	int y = 0;
	std::cout << "Please enter the point x-coordinate:" << std::endl;
	std::cin >> x;
	std::cout << "Please enter the point y-coordinate:" << std::endl;
	std::cin >> y;

	Point a(x, y);
	_shapes[shapeId - 1]->clearDraw(_canvas);
	_shapes[shapeId - 1]->move(a);
	_shapes[shapeId - 1]->draw(_canvas);
}
/*
This function prints info about a specific shape
*/
void Menu::getShapeInfo(int shapeId) {

	Shape* s = _shapes[shapeId - 1];
	std::cout << "Shape Type: " << s->getType() << std::endl;
	std::cout << "Shape Name: " << s->getName() << std::endl;
	std::cout << "Shape Area: " << std::to_string(s->getArea()) << std::endl;
	std::cout << "Shape Perimeter: " << std::to_string(s->getPerimeter()) << std::endl;

}
/*
This function deletes a shape, and clears it from the canvas
*/
void Menu::deleteShape(int shapeId) {

	_shapes[shapeId]->clearDraw(_canvas);
	_shapes.erase(_shapes.begin() + shapeId);

}
/*
This function deletes all the shapes
*/
void Menu::deleteAllShapes() {
	for (int i = 0; i < _shapes.size(); i++) {
		Menu::deleteShape(i);
	}
}
/*
This function exists from the program
*/
void Menu::ExitMenu() {
	std::cout << "Existing...";
	exit(1);
}