#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>
#include <iostream>


class Shape
{
public:

	// Constructor
	Shape(const std::string name, const std::string type);

	// Destructor
	~Shape();

	// Getters
	std::string getType() const;
	std::string getName() const;

	// Methods

	//those are pure virtual, because each class need to create its way of calculating things

	virtual double getArea() = 0;
	virtual double getPerimeter() = 0;
	virtual void move(const Point other) = 0; // aadd the Point to all the points of shape
	virtual void printDetails() = 0;

	virtual void draw(const Canvas& canvas) = 0;
	virtual void clearDraw(const Canvas& canvas) = 0;

private:
	std::string _name;
	std::string _type;
};