#include "Circle.h"

// Constructor, this function builts a new circle by its center and radius
Circle::Circle(const Point center, double radius, const std::string type, const std::string name) : Shape(name, type) {

	_center = center;
	_radius = radius;
}

// Destructor
Circle::~Circle() {
	delete& _center;
	delete& _radius;
}

// Getters
Point Circle::getCenter() const {
	return _center;
}
double Circle::getRadius() const {
	return _radius;
}

// This function calculates the circle's area
double Circle::getArea()
{
	return PI * _radius * _radius;
}
// This function calculates the circle's permimeter
double Circle::getPerimeter()
{
	return 2 * PI * _radius;
}
void Circle::move(Point other)
{ // add the Point to all the points of shape
	_center += other;
}
/*
This function prints details about the arrow
*/
void Circle::printDetails()
{
	std::cout << "Circle Data:\nCenter:" << std::endl;
	_center.printData();
	std::cout << "Radius:";
}



void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius());
}

void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}


