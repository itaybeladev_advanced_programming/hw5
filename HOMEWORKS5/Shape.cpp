#include "Shape.h"


// Constructor
Shape::Shape(const std::string name, const std::string type) {
	_type = type;
	_name = name;
}

// Destructor
Shape::~Shape() {
	_type.clear();
	_name.clear();
}

// Getters
std::string Shape::getType() const {
	return _type;
}
std::string Shape::getName() const {
	return _name;
}
