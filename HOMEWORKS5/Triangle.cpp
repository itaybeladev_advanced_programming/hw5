#include "Triangle.h"

// Constructor

/*
This function creates a new triangle by 3 given points
*/
Triangle::Triangle(const Point a, const Point b, const Point c, const std::string type, const std::string name) : Polygon(type, name) {
	Polygon::addPoint(a);
	Polygon::addPoint(b);
	Polygon::addPoint(c);

	if (getArea() == 0) {
		std::cout << "Error, invalid triangle";
		exit(1);
	}
}

// Destructor
Triangle::~Triangle() {}; // the Polygon destructor will delete everything.

// Methods

// override functions if need (virtual + pure virtual)

/*
This function calculates the area of the triangle by using a specific formula
*/
double Triangle::getArea() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];
	return abs(a.getX() * (b.getY() - c.getY()) + b.getX() * (c.getY() - a.getY()) + c.getX() * (a.getY() - b.getY()));
}

/*
This function calculates the perimeter of the triangle by summing up the distances
*/
double Triangle::getPerimeter() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];

	return a.distance(b) + b.distance(c) + c.distance(a);

}
/*
This function moves the triangle by a specific point
*/
void Triangle::move(Point other)
{
	for (int i = 0; i < TRIANGLE_SIDES; i++) {
		Polygon::changePoint(i, Polygon::getPoints()[i] + other);
	}
}
/*
This function prints data about the triangle
*/
void Triangle::printDetails()
{
	std::cout << "Triangle Data:" << std::endl;
	for (int i = 0; i < TRIANGLE_SIDES; i++) {
		Polygon::getPoints()[i].printData();
	}
}

void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2]);
}

void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}
