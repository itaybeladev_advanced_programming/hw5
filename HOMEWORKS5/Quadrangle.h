
#pragma once
#include "Polygon.h"
#define NUM_OF_SIDES 4

class Quadrangle : public Polygon
{
public:


	Quadrangle(Point a, Point b, Point c, Point d, const std::string type, const std::string name);

	// Destructor
	~Quadrangle();

	// Methods


	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// override functions if need (virtual + pure virtual)

	virtual double getArea() override;
	virtual double getPerimeter() override;
	virtual void move(Point other) override; // add the Point to all the points of shape
	virtual void printDetails() override;

};
