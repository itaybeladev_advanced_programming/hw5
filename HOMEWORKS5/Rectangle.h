#pragma once
#include "Polygon.h"
#define NUM_OF_SIDES 4

namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	public:

		// Constructor
		// There's a need only for the top left corner 
		Rectangle(const Point a, const double length, const double width, const std::string type, const std::string name);

		// Destructor
		~Rectangle();

		// Methods


		void draw(const Canvas& canvas) override;
		void clearDraw(const Canvas& canvas) override;

		// override functions if need (virtual + pure virtual)

		virtual double getArea() override;
		virtual double getPerimeter() override;
		virtual void move(Point other) override; // add the Point to all the points of shape
		virtual void printDetails() override;

	};
}