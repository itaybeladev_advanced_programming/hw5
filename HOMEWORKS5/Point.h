#pragma once

#include <math.h>
#include <string>
#include <iostream>

class Point
{
public:

	// Constructors
	Point(); // initialize values to 0
	Point(const double x, const double y);

	// Destructor
	~Point();

	// Operators
	Point operator+(Point& other);
	Point& operator+=(Point& other);
	bool operator==(const Point& other);
	// Getters
	double getX() const;
	double getY() const;
	void printData() const;
	// Methods
	double distance(const Point& other) const;
private:
	double _x;
	double _y;
};