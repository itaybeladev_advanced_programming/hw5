#include "Polygon.h"
#include "Shape.h"
#include <string>

#define TRIANGLE_SIDES 3

class Triangle : public Polygon
{
public:

	// Constructor
	Triangle(const Point a, const Point b, const Point c, const std::string type, const std::string name);

	// Destructor
	~Triangle();

	// Methods


	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

	// override functions if need (virtual + pure virtual)

	virtual double getArea() override;
	virtual double getPerimeter() override;
	virtual void move(Point other) override; // add the Point to all the points of shape
	virtual void printDetails() override;

};