#pragma once
#include "Quadrangle.h"


// Constructor

/*
This function creates a new quadrangle using 4 points
*/
Quadrangle::Quadrangle(Point a, Point b, Point c, Point d, const std::string type, const std::string name) : Polygon(type, name) {

	Polygon::addPoint(a);
	Polygon::addPoint(b);
	Polygon::addPoint(c);
	Polygon::addPoint(d);

}
/*
This function deletes a quadrangle
*/
Quadrangle::~Quadrangle() {
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		delete& Polygon::getPoints()[i];
	}
}; // the Polygon destructor will delete everything.


// Methods

// override functions if need (virtual + pure virtual)

/*
This function calculates the area of a quadrangle using a formula
*/
double Quadrangle::getArea() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];
	Point d = Polygon::getPoints()[3];
	return 0.5 * ((a.getX() * b.getY() + b.getX() * c.getY() + c.getX() * b.getY() + d.getX() * a.getY()) - (b.getX() * a.getY() + c.getX() * b.getY() + d.getX() * c.getY() + a.getX() * d.getY()));
}

/*
This function calculates the perimeter of the quadrangle by calculating the distance of each 2 points
*/
double Quadrangle::getPerimeter() {
	Point a = Polygon::getPoints()[0];
	Point b = Polygon::getPoints()[1];
	Point c = Polygon::getPoints()[2];
	Point d = Polygon::getPoints()[3];

	return a.distance(b) + b.distance(c) + c.distance(d) + d.distance(a);

}

/*
This function moves all the points by a given point
*/
void Quadrangle::move(Point other) {
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		Polygon::changePoint(i, Polygon::getPoints()[i] + other);
	}
}
/*
This function prints details about the quadrangle
*/
void Quadrangle::printDetails() {
	std::cout << "Quadrangle Data:" << std::endl;
	for (int i = 0; i < NUM_OF_SIDES; i++) {
		Polygon::getPoints()[i].printData();
	}
}

void Quadrangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1]);
}

void Quadrangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}


